#include <stdio.h>
#include "image_template.h"
#include "math.h"
#include <sys/time.h>
#include <string.h>
#include "omp.h"
#include "sort.h"

void Gaussian_Kernel(float ** G_Kernel, int *w, float Sigma){
	float a;
	float sum = 0;
	a = round(2.5*Sigma - 0.5);
	*w = 2*a + 1;
	*G_Kernel = (float *)malloc(sizeof(float) *(*w)); 
	for(int i=0; i<*w; i++){
		(*G_Kernel)[i] = exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
		sum += (*G_Kernel)[i];
	}
#pragma omp parallel for
	for(int i=0; i<*w; i++){
		(*G_Kernel)[i] = (*G_Kernel)[i]/sum;
	} 
}


void Gaussian_Deri_Kernel(float ** GD_Kernel, int *w, float Sigma){
	float a, sum=0;
	float temp;
	a = round(2.5*Sigma - 0.5);
	*w = 2*a+1;
	*GD_Kernel = (float*)malloc(sizeof(float)*(*w));
	for(int i=0; i<*w; i++){
		(*GD_Kernel)[i] = -1*(i-a)*exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
		sum -= i*(*GD_Kernel)[i];
	}
#pragma omp parallel for
	for(int i=0; i<*w; i++){
		(*GD_Kernel)[i] = (*GD_Kernel)[i]/sum;
		//printf("%f\n", (*GD_Kernel)[i]);
	}
	/*Kernel flipping*/ 
	for(int i=0; i<*w/2; i++){
		temp = (*GD_Kernel)[*w-1-i];
		(*GD_Kernel)[*w-1-i] = (*GD_Kernel)[i];
		(*GD_Kernel)[i] = temp;
	}
}


void Convolve(float *image, float ** I, int width, int height, int ker_h, int ker_w, float *kernel){
	int offseti, offsetj;
	*I = (float*)malloc(sizeof(float)*width*height);
	int j, k, m;
#pragma omp parallel for private(j,k,m)
	for(int i=0;i<height;i++){
		for(j=0;j<width;j++){
			float sum=0;
			for(k=0;k<ker_h;k++){
				for(m=0;m<ker_w;m++){
					offseti=-1*floor(ker_h/2)+k;
					offsetj=-1*floor(ker_w/2)+m;
					//
					if ( i+offseti>= 0 && i+offseti<height && j+offsetj >=0 && j+offsetj < width){
					sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
					}
				}
			}
			/* Output in a separate image */
			(*I)[i*width+j] = sum;
		}
	}
}


void magnitude(float ** mag, float *vertical, float *horizontal, int width, int height){
	*mag = (float*)malloc(sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width;j++){
			(*mag)[(i*width+j)] = sqrt(vertical[(i*width+j)]*vertical[(i*width+j)] + horizontal[(i*width+j)]*horizontal[(i*width+j)]);	
		}
	}
}


void direction(float ** dir, float *vertical, float *horizontal, int width, int height){
	*dir = (float*)malloc(sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width; j++){
			(*dir)[(i*width+j)] = atan2(horizontal[(i*width+j)], vertical[(i*width+j)]);
		}
	}
}

void suppression(float ** sup, float *mag, float *dir, int width, int height){
	*sup = (float*)malloc(sizeof(float)*width*height);
	memcpy(*sup, mag, sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width; j++){
			float langle = dir[i*width+j];
			if(langle<0){
				langle += M_PI;
			}
			langle = (180/M_PI) *langle;
			if(langle <= 22.5 || langle >157.5){
				// how to check the boundary? "and" or "or"
				if(i-1>=0 && i+1<=height){
					if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
						(*sup)[i*width+j] = 0;
					}	
				}
				
			}
			if(langle > 22.5 && langle <= 67.5){
				if((i-1>=0 && j-1>=0) && (i+1 <height && j+1 < width)){
					if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[((i+1)*width+j+1)] > mag[(i*width+j)]){
						(*sup)[i*width+j] = 0;
					}
				}
			}
			if(langle >67.5 && langle <=112.5){
				if(j-1>=0 && j+1 < width){
					if(mag[(i*width+j-1)] >mag[(i*width+j)] || mag[(i*width+j+1)] > mag[(i*width+j)]){
						(*sup)[i*width+j] = 0;
					}
				}
			}
			if(langle >112.5 && langle <= 157.5){
				if((i-1>=0 && j+1<width) && (i+1 <height && j-1 >=0)){
					if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[((i+1)*width+j-1)] > mag[(i*width+j)]){
						(*sup)[i*width+j] = 0;
					}
				}
			}
		}
	}
}




void hysteresis(float ** hys, float *sup, int width, int height){
	float t_high, t_low;	
	*hys = (float*)malloc(sizeof(float)*width*height);
	float *arr = (float*)malloc(sizeof(float)*width*height);
	memcpy(arr, sup, sizeof(float)*width*height);
	int arr_size = sizeof(arr)/sizeof(arr[0]);
	mergeSort(arr, 0, arr_size-1);
	t_high = arr[int(0.9 * width * height)];
	printf("\n%f", t_high);
	t_low = t_high / 5;
	memcpy(*hys, sup, sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width; j++){
			if(sup[(i*width+j)]>=t_high){
				(*hys)[(i*width+j)] = 255;
			}
			else if(sup[(i*width+j)]<=t_low){
				(*hys)[(i*width+j)] = 0;
			}
			else{
				(*hys)[(i*width+j)] = 125;
			} 
		}
	}
	free(arr);
	
}



void myedges(float ** edg, float *hys, int width, int height){
	*edg = (float*)malloc(sizeof(float)*width*height);
	memcpy(*edg, hys, sizeof(float)*width*height);
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			// For all pixels in Hyst image equal to 125 
			if(hys[i*width+j] == 125){
				(*edg)[i*width+j] = 0;
				for(int i_cord=-1; i_cord<2; i_cord++){
					for(int j_cord=-1; j_cord<2; j_cord++){
						// check the offset 
						if(i+i_cord>=0 && i+i_cord<height && j+j_cord>=0 && j+j_cord<width){
							if(hys[(i+i_cord)*width+j+j_cord] == 255){
								(*edg)[i*width+j] = 255;
							}
						}	
					}
				}	
			}
			
		}
	}
	
}

int main(int argc, char ** argv){
	int width, height;
	float *p; //image
	/* start the time counter */
	struct timeval start, end,compstart, compend;
	gettimeofday(&start, NULL);
	read_image_template<float>(argv[1], &p, &width, &height);
	/* set OpenMP environment */
	int threads = atoi(argv[3]);
	omp_set_num_threads(threads);
	
	gettimeofday(&compstart, NULL); // computational time 
	/* creat the Gaussian Kernel */
	float *G_Kernel;
	int w; 
	Gaussian_Kernel(&G_Kernel,&w,atof(argv[2]));
	/* creat the Gaussian Derivative Kernel */
	float *GD_Kernel;
	Gaussian_Deri_Kernel( &GD_Kernel, &w, atof(argv[2])); 
	
	/* Horizontal Gradient */ 
	float *temp_horizontal, *horizontal;
	Convolve(p, &temp_horizontal, width, height, w, 1, G_Kernel);	
	Convolve(temp_horizontal, &horizontal, width, height, 1, w, GD_Kernel);
	/* Vertical Gradient */
	float *temp_vertical, *vertical;
	Convolve(p, &temp_vertical, width, height, 1, w, G_Kernel);
	Convolve(temp_vertical, &vertical, width, height, w, 1, GD_Kernel);
	/* Magnitude */
	float *mag;
	magnitude(&mag, vertical, horizontal,width, height);
	/* Direction */
	float *dir;
	direction(&dir, vertical, horizontal,width, height);
	/* Non-maximum supression */
	float *sup;
	suppression(&sup, mag, dir, width, height);
	/* Hysteresis */
	float *hys;
	hysteresis(&hys, sup, width, height);
	/* Edges */
	float *edge;
	myedges(&edge, hys, width, height);
	/* end the computational time counter */
	gettimeofday(&compend, NULL);
	
	/* export the results */ 
	char hname[] ="horizontal.pgm";
	char vname[] ="vertical.pgm";
	char thname[] ="temp_horizontal.pgm";
	char tvname[] ="temp_vertical.pgm";
	char mname[] ="magnitude.pgm";
	char dname[] ="direction.pgm";
	char sname[] ="suppressed.pgm";
	char hyname[] ="hysteresis.pgm";
	char ename[] ="edge.pgm";
#pragma omp parallel sections
	{
		#pragma omp section
		write_image_template<float>(hname, horizontal, width, height);
		#pragma omp section
		write_image_template<float>(vname, vertical, width, height);
		#pragma omp section
		write_image_template<float>(thname, temp_horizontal, width, height);
		#pragma omp section
		write_image_template<float>(tvname, temp_vertical, width, height);
		#pragma omp section
		write_image_template<float>(mname, mag, width, height);
		#pragma omp section
		write_image_template<float>(dname, dir, width, height);
		#pragma omp section
		write_image_template<float>(sname, sup, width, height);
		#pragma omp section
		write_image_template<float>(hyname, hys, width, height);
		#pragma omp section
		write_image_template<float>(ename, edge, width, height);
	}
  	/* free the memory */
  	free(p);
  	free(G_Kernel);
  	free(GD_Kernel);
  	free(temp_horizontal);
  	free(horizontal);
  	free(temp_vertical);
  	free(vertical);
  	free(mag);
  	free(dir);
  	free(sup);
  	free(hys);
  	free(edge);
  	/* end of time count */
  	gettimeofday(&end, NULL);
  	/* print the time results million second */
	printf("Image size: %d, Sigma: %f, Threads: %d, computation time %ld, end to end time: %ld\n", width, atof(argv[2]), atoi(argv[3]), (((compend.tv_sec * 1000000 + compend.tv_usec) - (compstart.tv_sec * 1000000 + compstart.tv_usec)))/1000,(((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)))/1000);
  	return 0;
	}
