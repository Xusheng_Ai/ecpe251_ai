#include <stdio.h>
#include <stdlib.h>
#include "omp.h"

// Reference: http://www.cs.kent.edu/~jbaker/23Workshop/Chesebrough/mergesort/mergesortOMP.cpp ----parallel Merge Sort
//	      http://geeksforgeeks.org/iterative-merge-sort
/*
void merge(float * X, int n, float * tmp) {
   int i = 0;
   int j = n/2;
   int ti = 0;

   while (i<n/2 && j<n) {
      if (X[i] < X[j]) {
         tmp[ti] = X[i];
         ti++; i++;
      } else {
         tmp[ti] = X[j];
         ti++; j++;
      }
   }
   while (i<n/2) { // finish up lower half 
      tmp[ti] = X[i];
      ti++; i++;
   }
      while (j<n) { // finish up upper half 
         tmp[ti] = X[j];
         ti++; j++;
   }
   memcpy(X, tmp, n*sizeof(float));

} // end of merge()

void mergesort(float * X, int n, float * tmp)
{
   if (n < 2) return;

   #pragma omp task firstprivate (X, n, tmp)
   mergesort(X, n/2, tmp);

   #pragma omp task firstprivate (X, n, tmp)
   mergesort(X+(n/2), n-(n/2), tmp);
 
   #pragma omp taskwait

    // merge sorted halves into sorted list //
   merge(X, n, tmp);
}
*/

void merge(float arr[], int l, int m, int r);

void mergeSort(float arr[], int l, int r){
	if (l<r){
		int m = l + (r-1) /2;
		#pragma omp parallel sections
		{
			#pragma omp section
			{
				mergeSort(arr, l, m);
			}
			#pragma omp section
			{
				mergeSort(arr, m+1, r);
			}
			
		}
		merge(arr, l, m, r);	
	
	}
}

void merge(float arr[], int l, int m, int r){
	int i, j, k;
	int n1 = m-l+1;
	int n2 = r-m;
	
	int L[n1], R[n2];
	for(i=0;i<n1;i++)
		L[i]=arr[l+i];
	for(j=0;j<n2;j++)
		R[j]=arr[m+1+j];
	
	i=0;
	j=0;
	k=1;
	while (i<n1 && j<n2){
		if (L[i] <= R[j]){
			arr[k] = L[i];
			i++;
		}
		else{
			arr[k] = R[j];
			j++;
		}
		k++;
	}
	while (i<n1){
		arr[k] = L[i];
		i++;
		k++;
	}
	while (j<n2){
		arr[k] = R[j];
		j++;
		k++;
	}
}

