#include <stdio.h>
#include "image_template.h"
#include "math.h"
#include <sys/time.h>
#include <string.h>
#include "omp.h"
#include <mpi.h>

void Gaussian_Kernel(float ** G_Kernel, int *w, float Sigma){
	float a;
	float sum = 0;
	a = round(2.5*Sigma - 0.5);
	*w = 2*a + 1;
	*G_Kernel = (float *)malloc(sizeof(float) *(*w)); 
	for(int i=0; i<*w; i++){
		(*G_Kernel)[i] = exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
		sum += (*G_Kernel)[i];
	}
	for(int i=0; i<*w; i++){
		(*G_Kernel)[i] = (*G_Kernel)[i]/sum;
	} 
}


void Gaussian_Deri_Kernel(float ** GD_Kernel, int *w, float Sigma){
	float a, sum=0;
	float temp;
	a = round(2.5*Sigma - 0.5);
	*w = 2*a+1;
	*GD_Kernel = (float*)malloc(sizeof(float)*(*w));
	for(int i=0; i<*w; i++){
		(*GD_Kernel)[i] = -1*(i-a)*exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
		sum -= i*(*GD_Kernel)[i];
	}
	for(int i=0; i<*w; i++){
		(*GD_Kernel)[i] = (*GD_Kernel)[i]/sum;
	//	printf("%f\n", (*GD_Kernel)[i]);
	}
	/*Kernel flipping*/ 
	for(int i=0; i<*w/2; i++){
		temp = (*GD_Kernel)[*w-1-i];
		(*GD_Kernel)[*w-1-i] = (*GD_Kernel)[i];
		(*GD_Kernel)[i] = temp;
	}
}


void Convolve_h(float *image, float ** I, int width, int height, int ker_h, int ker_w, float *kernel, int comm_size, int comm_rank){	
	int offseti, offsetj;
	*I = (float*)malloc(sizeof(float)*width*height);
	int j, k, m;
#pragma omp parallel for private(j,k,m,offseti, offsetj) 	
	for(int i=0;i<height;i++){
		for(j=0;j<width;j++){
			float sum=0;
			for(k=0;k<ker_h;k++){
				for(m=0;m<ker_w;m++){
					offseti=-1*floor(ker_h/2)+k;
					offsetj=-1*floor(ker_w/2)+m;
	
					if ( j+offsetj >=0 && j+offsetj < width){
						sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
					}
				}
			}
				// Output in a separate image
				(*I)[i*width+j] = sum;
		}
	}
}

void Convolve_v(float *image, float ** I, int width, int height, int ker_h, int ker_w, float *kernel, int comm_size, int comm_rank){
	int a = floor(ker_h/2);
	*I = (float*)malloc(sizeof(float)*width*height);
	int offseti, offsetj, start;
	int j, k, m;
	if( comm_rank ==0){
		start = 0;
	}
	else{
		start = a;
	}
	#pragma omp parallel for private(j,k,m,offseti, offsetj) 
	for(int i=start;i<height+start;i++){
		for(j=0;j<width;j++){
			float sum=0;
			for(k=0;k<ker_h;k++){
				for(m=0;m<ker_w;m++){
					offseti=-1*floor(ker_h/2)+k;
					offsetj=-1*floor(ker_w/2)+m;
					if(comm_rank == 0){
						if(i+offseti >= 0){
							sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
						}
					}
					else if(comm_rank == comm_size-1){
						if(i+offseti < height + a ){
							sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
						}
					}
					else{
					
						sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
					}
				}
			}
			(*I)[(i-start)*width+j] = sum;
		}
	}
}
						

void magnitude(float ** mag, float *vertical, float *horizontal, int width, int height){
	*mag = (float*)malloc(sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width;j++){
			(*mag)[(i*width+j)] = sqrt(vertical[(i*width+j)]*vertical[(i*width+j)] + horizontal[(i*width+j)]*horizontal[(i*width+j)]);	
		}
	}
}


void direction(float ** dir, float *vertical, float *horizontal, int width, int height){
	*dir = (float*)malloc(sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width; j++){
			(*dir)[(i*width+j)] = atan2(horizontal[(i*width+j)], vertical[(i*width+j)]);
		}
	}
}

void suppression(float ** sup, float *mag, float *dir, int width, int height, int a, int comm_rank, int comm_size){
	int start;
	*sup = (float*)malloc(sizeof(float)*width*height);
	if( comm_rank ==0){
                start = 0;
		memcpy(*sup, mag, sizeof(float)*width*height);
         }
	else{
		start = a;
		memcpy(*sup, mag+a*width, sizeof(float)*width*height);
	}
	int j;
#pragma omp parallel for private(j)
	for(int i=start; i<height+start; i++){
		for(j=0; j<width; j++){
			float langle = dir[(i-start)*width+j];
			if(langle<0){
				langle += M_PI;
			}
			langle = (180/M_PI) *langle;
			if(langle <= 22.5 || langle >157.5){
				if (comm_rank == 0){
					if(i-1>=0){
						if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
							(*sup)[(i-start)*width+j] = 0;
						}
					}
				}
				else if (comm_rank == comm_size-1){
					if(i+1<height){
						if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
							(*sup)[(i-start)*width+j] = 0;
						}
					}
				}
				else{
					if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
						(*sup)[(i-start)*width+j] = 0;
					}
				}		
				
			}
			if(langle > 22.5 && langle <= 67.5){
				 if (comm_rank == 0){
                                        if((i-1>=0 && j-1>=0) && j+1 < width){
                                 		if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[((i+1)*width+j+1)] > mag[(i*width+j)]){
                                                        (*sup)[(i-start)*width+j] = 0;
                                                }
                                        }
                                }
				else if (comm_rank == comm_size-1){
                                        if(j-1>=0 && (i+1<height && j+1<width)){
                                                if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[((i+1)*width+j+1)] > mag[(i*width+j)]){
                                                        (*sup)[(i-start)*width+j] = 0;
                                                }
                                        }
                                }
				else {
					if(j-1>=0 && j+1<width){
						if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[((i+1)*width+j+1)] > mag[(i*width+j)]){
                                                        (*sup)[(i-start)*width+j] = 0;
                                                }
					}
				}	
			}
			if(langle >67.5 && langle <=112.5){
				if(j-1>=0 && j+1 < width){
					if(mag[(i*width+j-1)] >mag[(i*width+j)] || mag[(i*width+j+1)] > mag[(i*width+j)]){
						(*sup)[(i-start)*width+j] = 0;
					}
				}
			}
			if(langle >112.5 && langle <= 157.5){
				if (comm_rank == 0){
                                        if((i-1>=0 && j+1<width) && j-1 >= 0){
                                                if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[((i+1)*width+j-1)] > mag[(i*width+j)]){
                                                        (*sup)[(i-start)*width+j] = 0;
                                                }
                                        }
                                }
				else if (comm_rank == comm_size-1){
					if(j+1<width && (i+1 <height && j-1 >=0)){
                                 	       if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[((i+1)*width+j-1)] > mag[(i*width+j)]){
                                        	        (*sup)[(i-start)*width+j] = 0;
                                        	}
                                	}
				}
				else{
					if(j+1<width && j-1 >=0){
                                 	       if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[((i+1)*width+j-1)] > mag[(i*width+j)]){
                                        	        (*sup)[(i-start)*width+j] = 0;
                                        	}
                                	}
				}
			}
		}
	}
}

int comparator(const void * p1, const void * p2){
	float *vala, *valb;
	vala = (float *)p1;
	valb = (float *)p2;
	
	if( *vala < *valb){
		return -1;
	}
	else{
		return (*vala >= * valb);
	}
}


void hysteresis(float ** hys, float *sup, int width, int height, float t_high, float t_low){	
	*hys = (float*)malloc(sizeof(float)*width*height);
	memcpy(*hys, sup, sizeof(float)*width*height);
	int j;
#pragma omp parallel for private(j)
	for(int i=0; i<height; i++){
		for(j=0; j<width; j++){
			if(sup[(i*width+j)]>=t_high){
				(*hys)[(i*width+j)] = 255;
			}
			else if(sup[(i*width+j)]<=t_low){
				(*hys)[(i*width+j)] = 0;
			}
			else{
				(*hys)[(i*width+j)] = 125;
			} 
		}
	}
	
}



void myedges(float ** edg, float *hys, int width, int height, int a, int comm_rank, int comm_size){
	int start;
	*edg = (float*)malloc(sizeof(float)*width*height);
        if( comm_rank ==0){
                start = 0;
                memcpy(*edg, hys, sizeof(float)*width*height);
         }
        
        else{
                start = a;
                memcpy(*edg, hys+width*a, sizeof(float)*width*height);
        }

	for(int i=start; i<height+start; i++){
		for(int j=0; j<width; j++){
			// For all pixels in Hyst image equal to 125 
			if(hys[i*width+j] == 125){
				(*edg)[(i-start)*width+j] = 0;
				for(int i_cord=-1; i_cord<2; i_cord++){
					for(int j_cord=-1; j_cord<2; j_cord++){
						// check the offset
						if( comm_rank == 0){
							if(i+i_cord>=0 && j+j_cord>=0 && j+j_cord<width){
								if(hys[(i+i_cord)*width+j+j_cord] == 255){
									(*edg)[(i-start)*width+j] = 255;
								}
							}
						}
						else if (comm_rank == comm_size-1){
							if(i+i_cord<height && j+j_cord>=0 && j+j_cord<width){
								if(hys[(i+i_cord)*width+j+j_cord] == 255){
									(*edg)[(i-start)*width+j] = 255;
								}
							}
						} 
						else{
							if(j+j_cord>=0 && j+j_cord<width){
					
								if(hys[(i+i_cord)*width+j+j_cord] == 255){
									(*edg)[(i-start)*width+j] = 255;
								}
							}
						}	
					}
				}	
			}
			
		}
	}
	
}

float *SendReceive (float *image_chunk, int cheight, int cwidth, int a, int comm_rank, int comm_size){
	float *rows_from_above, *rows_from_below;
	float *chunk;
	// step 1 
	if(comm_rank == 0 || comm_rank == (comm_size-1)){
                chunk  = (float*)malloc(sizeof(float)*cwidth*(cheight+a));
        }
        else{
                chunk = (float*)malloc(sizeof(float)*cwidth*(cheight+2*a));
        }
        if(comm_rank == 0){

                memcpy(chunk, image_chunk, sizeof(float)*cwidth*cheight);
        }
        else{
                memcpy(chunk+cwidth*a, image_chunk, sizeof(float)*cwidth*cheight);
        }
	// step 2
	rows_from_below = (float*)malloc(sizeof(float)*cwidth*a);
	rows_from_above = (float*)malloc(sizeof(float)*cwidth*a);
	MPI_Status status;
	if(comm_rank == 0){
		
		MPI_Sendrecv(image_chunk+cwidth*(cheight-a), a*cwidth, MPI_FLOAT, comm_rank+1, comm_rank, rows_from_below, a*cwidth, MPI_FLOAT, comm_rank+1, comm_rank+1, MPI_COMM_WORLD, &status);
	}
	else if (comm_rank == (comm_size-1)){
		MPI_Sendrecv(image_chunk, a*cwidth, MPI_FLOAT, comm_rank-1, comm_rank, rows_from_above, a*cwidth, MPI_FLOAT, comm_rank-1, comm_rank-1, MPI_COMM_WORLD, &status);	
		
	}
	else{
		MPI_Sendrecv(image_chunk+cwidth*(cheight-a), a*cwidth, MPI_FLOAT, comm_rank+1, comm_rank, rows_from_below, a*cwidth, MPI_FLOAT, comm_rank+1, comm_rank+1, MPI_COMM_WORLD, &status);
		MPI_Sendrecv(image_chunk, a*cwidth, MPI_FLOAT, comm_rank-1, comm_rank, rows_from_above, a*cwidth, MPI_FLOAT, comm_rank-1, comm_rank-1, MPI_COMM_WORLD, &status);
	}
	if(comm_rank == 0)
                memcpy(chunk+cwidth*cheight, rows_from_below, sizeof(float)*cwidth*a);
        else if (comm_rank == comm_size-1)
                memcpy(chunk, rows_from_above, sizeof(float)*cwidth*a);
        else{
                memcpy(chunk, rows_from_above, sizeof(float)*cwidth*a);
                memcpy(chunk+cwidth*(cheight+a), rows_from_below, sizeof(float)*cwidth*a);
        }

	return chunk;	
}


int main(int argc, char ** argv){
	int width, height;
	float *p; //image
	float *G_Kernel, *GD_Kernel;
	int w;
	float *horizontal,*vertical, *mag, *dir, *sup, *hys, *edg;
	/* set OpenMP environment */
	int threads = atoi(argv[3]);
	omp_set_num_threads(threads);
	/* Initialize MPI */
	int comm_size, comm_rank, rc;
	MPI_Init(&argc, &argv);
	/* rc  = MPI_Init(&argc, &argv);
	if(rc != MPI_SUCCESS)
		MPI_Abort(MPI_COMM_WORLD, rc);
	*/
	// get individual ranks
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
		
	struct timeval start, end, compstart, compend;
	
	if(comm_rank == 0){
		gettimeofday(&start, NULL); 
		read_image_template<float>(argv[1], &p, &width, &height);
		horizontal = (float*)malloc(sizeof(float)*width*height);
		vertical = (float*)malloc(sizeof(float)*width*height);
		mag = (float*)malloc(sizeof(float)*width*height);
		dir = (float*)malloc(sizeof(float)*width*height);
		sup = (float*)malloc(sizeof(float)*width*height);
		hys = (float*)malloc(sizeof(float)*width*height);
		edg = (float*)malloc(sizeof(float)*width*height);
		gettimeofday(&compstart, NULL); // computational time 
	}
	
	/* creat the Gaussian Kernel */ 
	Gaussian_Kernel(&G_Kernel,&w,atof(argv[2]));
	/* creat the Gaussian Derivative Kernel */
	Gaussian_Deri_Kernel( &GD_Kernel, &w, atof(argv[2])); 
	
	MPI_Bcast(&width, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&height,1,MPI_INT,0,MPI_COMM_WORLD);
	// all processors allocate their respective chunks
	int cwidth, cheight, a;
	float *image_chunk;
	cwidth = width;
	cheight = height/comm_size;
	a = floor(w/2);
	image_chunk = (float *)malloc(sizeof(float)*width*(height/comm_size));
	// scatter away
	MPI_Scatter(p, width*(height/comm_size), MPI_FLOAT,image_chunk, width*(height/comm_size), MPI_FLOAT,0,MPI_COMM_WORLD);
	float *chunk;
	chunk = SendReceive(image_chunk, cheight, cwidth, a, comm_rank, comm_size);

	/* Horizontal Gradient */ 
	float *temp_horizontal, *horizontal_chunk;
	Convolve_v(chunk, &temp_horizontal, cwidth, cheight, w, 1, G_Kernel, comm_size, comm_rank);	
	Convolve_h(temp_horizontal, &horizontal_chunk, cwidth, cheight, 1, w, GD_Kernel, comm_size, comm_rank);
	MPI_Gather(horizontal_chunk, cwidth*cheight,MPI_FLOAT,horizontal,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	
	/* Vertical Gradient */ 
	float *temp_vertical,*vertical_chunk, *temp_vertical_chunk;
	Convolve_h(image_chunk, &temp_vertical, cwidth, cheight, 1, w, G_Kernel, comm_size, comm_rank);
	temp_vertical_chunk = SendReceive(temp_vertical, cheight, cwidth, a, comm_rank, comm_size);
	Convolve_v(temp_vertical_chunk, &vertical_chunk, cwidth, cheight, w, 1, GD_Kernel, comm_size, comm_rank);
	MPI_Gather(vertical_chunk, cwidth*cheight,MPI_FLOAT,vertical,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	/* Magnitude */ 
	float *t_mag;
	magnitude(&t_mag, vertical_chunk, horizontal_chunk,cwidth, cheight);
	MPI_Gather(t_mag, cwidth*cheight,MPI_FLOAT,mag,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	/* Direction */
	float *t_dir;
	direction(&t_dir, vertical_chunk, horizontal_chunk,cwidth, cheight);
	MPI_Gather(t_dir, cwidth*cheight,MPI_FLOAT,dir,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	/* Non-maximum supression */
	float *t_sup, *mag_chunk;
	mag_chunk = SendReceive(t_mag, cheight, cwidth, a, comm_rank, comm_size);
	suppression(&t_sup, mag_chunk, t_dir, cwidth, cheight, a, comm_rank, comm_size);
	MPI_Gather(t_sup, cwidth*cheight,MPI_FLOAT,sup,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);	
	/* Hysteresis */ 
	float *t_hys;
	float t_high, t_low;
	if (comm_rank == 0){
		float *arr = (float*)malloc(sizeof(float)*width*height);
		memcpy(arr, sup, sizeof(float)*width*height);
		qsort(arr, width*height, sizeof(float), comparator);
		t_high = arr[int(0.9*width*height)];
		t_low = t_high / 5;
	//	printf("\n%f", t_high);
		
	}
	MPI_Bcast(&t_high, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&t_low,1,MPI_FLOAT,0,MPI_COMM_WORLD);
	
	hysteresis(&t_hys, t_sup, cwidth, cheight, t_high, t_low);
	MPI_Gather(t_hys, cwidth*cheight,MPI_FLOAT,hys,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	/* Edges */
	float *t_edg, *hys_chunk;
	hys_chunk = SendReceive(t_hys, cheight, cwidth, a, comm_rank, comm_size);
	myedges(&t_edg, hys_chunk, cwidth, cheight, a, comm_rank, comm_size);
	MPI_Gather(t_edg, cwidth*cheight,MPI_FLOAT,edg,cwidth*cheight,MPI_FLOAT,0,MPI_COMM_WORLD);
	/* end the computational time counter */
	gettimeofday(&compend, NULL);
	free(hys_chunk);
	free(t_edg);
	free(G_Kernel);
	free(GD_Kernel);
	free(image_chunk);
	free(chunk);
	free(temp_horizontal);
	free(horizontal_chunk);
	free(temp_vertical);
	free(vertical_chunk);
	free(temp_vertical_chunk);
	free(t_mag);
	free(t_dir);
	free(t_sup);
	free(mag_chunk);
	free(t_hys);
		
	/* export the results */ 
	char hname[] ="horizontal.pgm";
	char vname[] ="vertical.pgm";
	char mname[] ="magnitude.pgm";
	char dname[] ="direction.pgm";
	char sname[] ="suppressed.pgm";
	char hyname[] ="hysteresis.pgm";
	char ename[] ="edge.pgm";
	if (comm_rank == 0){
		/* end the computational time counter */
	        gettimeofday(&compend, NULL);
		/*
		write_image_template<float>(hname, horizontal, width, height);
		write_image_template<float>(vname, vertical, width, height);
		write_image_template<float>(mname, mag, width, height);
		write_image_template<float>(dname, dir, width, height);
		write_image_template<float>(sname, sup, width, height);
		write_image_template<float>(hyname, hys, width, height);
		write_image_template<float>(ename, edg, width, height);
		*/
		/* free the memory */
		free(p);
		free(horizontal);
		free(vertical);
        	free(mag);
        	free(dir);
        	free(sup);
        	free(hys);
        	free(edg);
		/* end of time count */
		gettimeofday(&end, NULL);

		/* print the time results million second */
		printf("%d, %f, %d, %d, %ld, %ld\n", width, atof(argv[2]),atoi(argv[3]), comm_size, (((compend.tv_sec * 1000000 + compend.tv_usec) - (compstart.tv_sec * 1000000 + compstart.tv_usec)))/1000,(((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)))/1000);
	}
  	MPI_Finalize();
	return 0;
}
