#!/bin/bash
##SBATCH --partition=compute   ### Partition
#SBATCH --job-name=PA5       ### Job Name
#SBATCH --time=03:00:00     ### WallTime
#SBATCH --nodes=1          ### Number of Nodes
#SBATCH --tasks-per-node=1 ### Number of tasks (MPI processes=nodes*tasks-per-node. In this case, 32.

for i in 256 512 1024 2048 4096 8192 10240; do
	for j in 8 16 32; do
		for((a=0;a<30;a++)) do #30 statistical runs
			srun ./Canny_Edge ~/Lenna_image/Lenna_org_$i.pgm 0.6 $j
		done
	done
done
