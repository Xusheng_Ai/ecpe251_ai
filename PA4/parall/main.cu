#include <cuda.h>
#include <stdlib.h>
#include "image_template.h"
#include <sys/time.h>
#include <stdio.h>
#include "math.h"

void Gaussian_Kernel(float ** G_Kernel, int *w, float Sigma){
        float a;
        float sum = 0;
        a = round(2.5*Sigma - 0.5);
        *w = 2*a + 1;
        *G_Kernel = (float *)malloc(sizeof(float) *(*w));
        for(int i=0; i<*w; i++){
                (*G_Kernel)[i] = exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
                sum += (*G_Kernel)[i];
        }
        for(int i=0; i<*w; i++){
                (*G_Kernel)[i] = (*G_Kernel)[i]/sum;
        }
}


void Gaussian_Deri_Kernel(float ** GD_Kernel, int *w, float Sigma){
        float a, sum=0;
        float temp;
        a = round(2.5*Sigma - 0.5);
        *w = 2*a+1;
        *GD_Kernel = (float*)malloc(sizeof(float)*(*w));
        for(int i=0; i<*w; i++){
                (*GD_Kernel)[i] = -1*(i-a)*exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
                sum -= i*(*GD_Kernel)[i];
        }
        for(int i=0; i<*w; i++){
                (*GD_Kernel)[i] = (*GD_Kernel)[i]/sum;
               // printf("%f\n", (*GD_Kernel)[i]);
        }
        /*Kernel flipping*/
        for(int i=0; i<*w/2; i++){
                temp = (*GD_Kernel)[*w-1-i];
                (*GD_Kernel)[*w-1-i] = (*GD_Kernel)[i];
                (*GD_Kernel)[i] = temp;
        }
}

/* Optimized Convolve */
/*
__global__
void Convolve(float *image, float *I, int width, int height, int ker_h, int ker_w, float *kernel){
        int offseti, offsetj;
        int localidx = threadIdx.x;
	int localidy = threadIdx.y;
	int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;

	extern __shared__ float A[];
	A[localidx*blockDim.y+localidy] = image[i*width+j];
	__syncthreads();
        if(i<height && j< width){
                float sum=0;
                for(int k=0; k<ker_h;k++){
                        for(int m=0;m<ker_w;m++){
                                offseti=-1* floorf( ker_h/2)+k;
                                offsetj=-1* floorf( ker_w/2)+m;
                                if ( localidx+offseti>= 0 && localidx+offseti<blockDim.x && localidy+offsetj >=0 && localidy+offsetj < blockDim.y){
                                        sum += A[(localidx+offseti)*blockDim.y+localidy+offsetj] * kernel[(k*ker_w+m)];
                                }
				else if(i+offseti>= 0 && i+offseti<height && j+offsetj >=0 && j+offsetj < width) {
					sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
				}
                        }
                }
                I[i*width+j]=sum;
        }
}
*/
/* naive convolve */

__global__
void Convolve(float *image, float *I, int width, int height, int ker_h, int ker_w, float *kernel){
        int offseti, offsetj;
        int i = threadIdx.x + blockIdx.x*blockDim.x; 
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	
	if(i<height && j< width){
		float sum=0;
		for(int k=0; k<ker_h;k++){
			for(int m=0;m<ker_w;m++){
				offseti=-1* floorf( ker_h/2)+k;
				offsetj=-1* floorf( ker_w/2)+m;
				if ( i+offseti>= 0 && i+offseti<height && j+offsetj >=0 && j+offsetj < width){
					sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
				}
			}
		}
		I[i*width+j]=sum;
	}
}

__global__
void magnitude(float * mag, float *vertical, float *horizontal, int width, int height){
        int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if ( i<height && j<width) {
		mag[i*width+j] = sqrt(vertical[(i*width+j)]*vertical[(i*width+j)] + horizontal[(i*width+j)]*horizontal[(i*width+j)]);
	}	
}


__global__
void direction(float *dir, float *vertical, float *horizontal, int width, int height){
        int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if(i<height && width){
		dir[(i*width+j)] = atan2(vertical[(i*width+j)], horizontal[(i*width+j)]);
	}
}


int main(int argc, char ** argv){
        int width, height;
        float *h_image, *h_mag, *h_dir;
	float *d_image, *dG_Kernel, *dGD_Kernel;
        float *dtemp_horizontal, *d_horizontal, *dtemp_vertical, *d_vertical;
	float *d_mag, *d_dir;
	read_image_template<float>(argv[1], &h_image, &width, &height);
	/* allocates GPU's buffers 
	cudaMalloc((void **)&d_image, sizeof(float)*width*height);
	/* host transfers image to GPU for processing 
	cudaMemcpy(d_image, h_image, sizeof(float)*width*height, cudaMemcpyHostToDevice);*/
        /* start the time counter */
        struct timeval start, stop, convstart, convstop;
        //gettimeofday(&start, NULL);
        /* creat the Gaussian Kernel */
        float *G_Kernel;
        int w;
        Gaussian_Kernel(&G_Kernel,&w,atof(argv[2]));
        /* creat the Gaussian Derivative Kernel */
        float *GD_Kernel;
        Gaussian_Deri_Kernel( &GD_Kernel, &w, atof(argv[2]));
	/* allocates host's and GPU's buffers */
	h_mag = (float*)malloc(sizeof(float)*width*height);
	h_dir = (float*)malloc(sizeof(float)*width*height);

	cudaMalloc((void **)&d_image, sizeof(float)*width*height);
	cudaMalloc((void **)&dG_Kernel, sizeof(float)*w);
	cudaMalloc((void **)&dGD_Kernel, sizeof(float)*w);
	cudaMalloc((void **)&dtemp_horizontal, sizeof(float)*width*height);
	cudaMalloc((void **)&d_horizontal, sizeof(float)*width*height);
	cudaMalloc((void **)&dtemp_vertical, sizeof(float)*width*height);
	cudaMalloc((void **)&d_vertical, sizeof(float)*width*height);
	cudaMalloc((void **)&d_mag,sizeof(float)*width*height);
	cudaMalloc((void **)&d_dir,sizeof(float)*width*height);
	
	/* host transfers data to GPU for processing */
	struct timeval commstart0,commstop0;
	gettimeofday(&commstart0, NULL);
	cudaMemcpy(d_image, h_image, sizeof(float)*width*height, cudaMemcpyHostToDevice);
	cudaMemcpy(dG_Kernel, G_Kernel, sizeof(float)*w, cudaMemcpyHostToDevice);
	cudaMemcpy(dGD_Kernel, GD_Kernel, sizeof(float)*w, cudaMemcpyHostToDevice);
        gettimeofday(&commstop0,NULL);
	/* call the GPU kernel for processing */
	int blocksize = atoi(argv[3]);
	dim3 dimBlock(blocksize,blocksize);
	dim3 dimGrid(width/blocksize, height/blocksize);
	gettimeofday(&start, NULL); 
	gettimeofday(&convstart,NULL);
	/* Horizontal Gradient /
	Convolve<<<dimGrid, dimBlock>>>(d_image, dtemp_horizontal,width,height,w,1,dG_Kernel);
	Convolve<<<dimGrid, dimBlock>>>(dtemp_horizontal,d_horizontal,width,height,1,w,dGD_Kernel);
        cudaDeviceSynchronize();
	/* Vertical Gradient /
        Convolve<<<dimGrid,dimBlock>>>(d_image, dtemp_vertical,width,height,1,w,dG_Kernel);
	Convolve<<<dimGrid,dimBlock>>>(dtemp_vertical, d_vertical,width,height,w,1,dGD_Kernel);
        cudaDeviceSynchronize();
	gettimeofday(&convstop,NULL);
	*/
	/* share memory */
	
	gettimeofday(&convstart,NULL);
	Convolve<<<dimGrid, dimBlock,sizeof(float)*blocksize*blocksize >>>(d_image, dtemp_horizontal,width,height,w,1,dG_Kernel);
        Convolve<<<dimGrid, dimBlock,sizeof(float)*blocksize*blocksize>>>(dtemp_horizontal,d_horizontal,width,height,1,w,dGD_Kernel);
	cudaDeviceSynchronize();
	Convolve<<<dimGrid,dimBlock,sizeof(float)*blocksize*blocksize>>>(d_image, dtemp_vertical,width,height,1,w,dG_Kernel);
        Convolve<<<dimGrid,dimBlock,sizeof(float)*blocksize*blocksize>>>(dtemp_vertical, d_vertical,width,height,w,1,dGD_Kernel);
        cudaDeviceSynchronize();
        gettimeofday(&convstop,NULL);
		
	/* Magnitude */
        struct timeval magstart,magstop;
        gettimeofday(&magstart, NULL);
        magnitude<<<dimGrid,dimBlock>>>(d_mag, d_vertical, d_horizontal,width,height);
	struct timeval commstart1,commstop1;
        gettimeofday(&commstart1, NULL);
	cudaMemcpy(h_mag,d_mag,sizeof(float)*width*height,cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();
	gettimeofday(&commstop1,NULL);
        gettimeofday(&magstop,NULL);
        /* Direction */
	struct timeval dirstart,dirstop;
        gettimeofday(&dirstart, NULL);
        direction<<<dimGrid,dimBlock>>>(d_dir, d_vertical, d_horizontal,width, height);
	struct timeval commstart2,commstop2;
        gettimeofday(&commstart2, NULL);
        cudaMemcpy(h_dir,d_dir,sizeof(float)*width*height,cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();
	gettimeofday(&commstop2, NULL);
        gettimeofday(&dirstop,NULL);
	gettimeofday(&stop, NULL);
        /* export the results */
	char hname[] ="horizontal.pgm";
        char vname[] ="vertical.pgm";
        char thname[] ="temp_horizontal.pgm";
        char tvname[] ="temp_vertical.pgm";
        char mname[] ="magnitude.pgm";
        char dname[] ="direction.pgm";
        /*
	write_image_template<float>(hname, horizontal, width, height);
        write_image_template<float>(vname, vertical, width, height);
        write_image_template<float>(thname, temp_horizontal, width, height);
        write_image_template<float>(tvname, temp_vertical, width, height);
        */
	struct timeval fiostart,fiostop;
        gettimeofday(&fiostart, NULL);
	write_image_template<float>(mname, h_mag, width, height);
        write_image_template<float>(dname, h_dir, width, height);
	gettimeofday(&fiostop, NULL);
	/* end the time counter */
        //gettimeofday(&stop, NULL);
	/* free the memory */
        free(h_image);
        free(G_Kernel);
        free(GD_Kernel);
	
        free(h_mag);
        free(h_dir);
        cudaFree(d_image);
	cudaFree(dG_Kernel);
	cudaFree(dGD_Kernel);
	cudaFree(dtemp_horizontal);
	cudaFree(d_horizontal);
	cudaFree(dtemp_vertical);
	cudaFree(d_vertical);
	/* print the time results million second */
        //printf("%d, %f, %ld, %ld, %ld, %ld, %ld\n", width, atof(argv[2]), (((convstop.tv_sec * 1000000 + convstop.tv_usec) - (convstart.tv_sec * 1000000 + convstart.tv_usec)))/1000,(((magstop.tv_sec * 1000000 + magstop.tv_usec) - (magstart.tv_sec * 1000000 + magstart.tv_usec))+ ((dirstop.tv_sec * 1000000 + dirstop.tv_usec) - (dirstart.tv_sec * 1000000 + dirstart.tv_usec)))/1000, (((commstop0.tv_sec * 1000000 + commstop0.tv_usec) - (commstart0.tv_sec * 1000000 + commstart0.tv_usec))+ ((commstop1.tv_sec * 1000000 + commstop1.tv_usec) - (commstart1.tv_sec * 1000000 + commstart1.tv_usec))+((commstop2.tv_sec * 1000000 + commstop2.tv_usec) - (commstart2.tv_sec * 1000000 + commstart2.tv_usec)))/1000,(((fiostop.tv_sec * 1000000 + fiostop.tv_usec) - (fiostart.tv_sec * 1000000 + fiostart.tv_usec)))/1000,(((stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)))/1000);
	//printf("%d,%f,%ld\n", width, atof(argv[2]),(((convstop.tv_sec * 1000000 + convstop.tv_usec) - (convstart.tv_sec * 1000000 + convstart.tv_usec)))/1000);
	//printf("%d,%f,%ld\n", width, atof(argv[2]),(((stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)))/1000);
	printf("%d,%f,%ld,%ld\n", width, atof(argv[2]),(((commstop0.tv_sec * 1000000 + commstop0.tv_usec) - (commstart0.tv_sec * 1000000 + commstart0.tv_usec)))/1000, (((commstop1.tv_sec * 1000000 + commstop1.tv_usec) - (commstart1.tv_sec * 1000000 + commstart1.tv_usec))+((commstop2.tv_sec * 1000000 + commstop2.tv_usec) - (commstart2.tv_sec * 1000000 + commstart2.tv_usec)))/1000);
	return 0;
}
