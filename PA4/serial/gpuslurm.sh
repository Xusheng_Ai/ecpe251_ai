#!/bin/bash
##SBATCH --partition=compute   ### Partition
#SBATCH --job-name=PA4       ### Job Name
#SBATCH --time=03:00:00     ### WallTime
#SBATCH --nodes=1          ### Number of Nodes
#SBATCH --tasks-per-node=1 ### Number of tasks (MPI processes=nodes*tasks-per-node. In this case, 32.

for i in 1024 2048 4096 7680 10240; do
	for j in 0.6 1.1; do
		for((a=0;a<1;a++)) do #30 statistical runs
			srun ./Canny_Edge ~/Lenna_image/Lenna_org_$i.pgm $j
		done
	done
done
