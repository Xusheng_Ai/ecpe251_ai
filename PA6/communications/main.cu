#include <cuda.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

int main(){
        cudaSetDevice(989355990%4);
        float *CPUarray, *GPUarray;
	struct timeval start1,start2, stop1,stop2;
	float H2Dtime, D2Htime;
	for(int i=1;i<=524288;i=i*2){
		int size = i * 1024;
		CPUarray = (float*)malloc(sizeof(float)*size);
		cudaMalloc((void **)&GPUarray,sizeof(float)*size);
		H2Dtime=0;
		D2Htime=0;
		for(int j=0; j<30; j++){
			gettimeofday(&start1, NULL);
			cudaMemcpy(GPUarray, CPUarray, sizeof(float)*size, cudaMemcpyHostToDevice);
			cudaDeviceSynchronize();
			gettimeofday(&stop1, NULL);
			H2Dtime += (stop1.tv_sec * 1000000 + stop1.tv_usec) - (start1.tv_sec * 1000000 + start1.tv_usec);
			gettimeofday(&start2, NULL);
			cudaMemcpy(CPUarray, GPUarray, sizeof(float)*size,cudaMemcpyDeviceToHost);
			cudaDeviceSynchronize();
			gettimeofday(&stop2,NULL);
			D2Htime += (stop2.tv_sec * 1000000 + stop2.tv_usec) - (start2.tv_sec * 1000000 + start2.tv_usec);
		}	

		float H2Davg = H2Dtime/30;
		float H2Dbandwidth =  size/H2Davg;
		float D2Havg = D2Htime/30;
		float D2Hbandwidth = size/D2Havg; 
  		printf("%d,%f,%f\n",i, H2Dbandwidth, D2Hbandwidth); 
		cudaFree(GPUarray);
		free(CPUarray);
	}
}
