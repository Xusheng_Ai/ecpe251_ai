#!/bin/bash
##SBATCH --partition=compute   ### Partition
#SBATCH --job-name=PA6       ### Job Name
#SBATCH --time=03:00:00     ### WallTime
#SBATCH --nodes=1          ### Number of Nodes
#SBATCH --tasks-per-node=1 ### Number of tasks (MPI processes=nodes*tasks-per-node. In this case, 32.

srun ./benchmark
