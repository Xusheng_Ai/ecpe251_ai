#include <cuda.h>
#include <stdlib.h>
#include "image_template.h"
#include <sys/time.h>
#include <stdio.h>
#include "math.h"
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>

void Gaussian_Kernel(float ** G_Kernel, int *w, float Sigma){
        float a;
        float sum = 0;
        a = round(2.5*Sigma - 0.5);
        *w = 2*a + 1;
        *G_Kernel = (float *)malloc(sizeof(float) *(*w));
        for(int i=0; i<*w; i++){
                (*G_Kernel)[i] = exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
                sum += (*G_Kernel)[i];
        }
        for(int i=0; i<*w; i++){
                (*G_Kernel)[i] = (*G_Kernel)[i]/sum;
        }
}


void Gaussian_Deri_Kernel(float ** GD_Kernel, int *w, float Sigma){
        float a, sum=0;
        float temp;
        a = round(2.5*Sigma - 0.5);
        *w = 2*a+1;
        *GD_Kernel = (float*)malloc(sizeof(float)*(*w));
        for(int i=0; i<*w; i++){
                (*GD_Kernel)[i] = -1*(i-a)*exp((-1*(i-a)*(i-a))/(2*Sigma*Sigma));
                sum -= i*(*GD_Kernel)[i];
        }
        for(int i=0; i<*w; i++){
                (*GD_Kernel)[i] = (*GD_Kernel)[i]/sum;
               
        }
        /*Kernel flipping*/
        for(int i=0; i<*w/2; i++){
                temp = (*GD_Kernel)[*w-1-i];
                (*GD_Kernel)[*w-1-i] = (*GD_Kernel)[i];
                (*GD_Kernel)[i] = temp;
        }
}


/* Optimized Convolve */
__global__
void Convolve(float *image, float *I, int width, int height, int ker_h, int ker_w, float *kernel){
        int offseti, offsetj;
        int localidx = threadIdx.x;
        int localidy = threadIdx.y;
        int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;

        extern __shared__ float A[];
        A[localidx*blockDim.y+localidy] = image[i*width+j];
        __syncthreads();
        if(i<height && j< width){
                float sum=0;
                for(int k=0; k<ker_h;k++){
                        for(int m=0;m<ker_w;m++){
                                offseti=-1* floorf( ker_h/2)+k;
                                offsetj=-1* floorf( ker_w/2)+m;
                                if ( localidx+offseti>= 0 && localidx+offseti<blockDim.x && localidy+offsetj >=0 && localidy+offsetj < blockDim.y){
                                        sum += A[(localidx+offseti)*blockDim.y+localidy+offsetj] * kernel[(k*ker_w+m)];
                                }
                                else if(i+offseti>= 0 && i+offseti<height && j+offsetj >=0 && j+offsetj < width) {
                                        sum += image[(i+offseti)*width+j+offsetj] * kernel[(k*ker_w+m)];
                                }
                        }
                }
                I[i*width+j]=sum;
        }
}


__global__
void magnitude(float * mag, float *vertical, float *horizontal, int width, int height){
        int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;
        if ( i<height && j<width) {
                mag[i*width+j] = sqrt(vertical[(i*width+j)]*vertical[(i*width+j)] + horizontal[(i*width+j)]*horizontal[(i*width+j)]);
        }
}


__global__
void direction(float *dir, float *vertical, float *horizontal, int width, int height){
        int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;
        if(i<height && width){
                dir[(i*width+j)] = atan2(horizontal[(i*width+j)], vertical[(i*width+j)]);
        }
}


__global__
void suppression(float *sup, float *mag, float *dir, int width, int height){
        int localidx = threadIdx.x;
        int localidy = threadIdx.y;
        int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;
	
	sup[i*width+j] = mag[i*width+j];
	extern __shared__ float A[];
        A[localidx*blockDim.y+localidy] = mag[i*width+j];
        __syncthreads();
	
	if(i<height && j< width){
		float langle = dir[i*width+j];
		if(langle<0){
                	langle += M_PI;
                }
                langle = (180/M_PI) *langle;
	/*	
		if(langle <= 22.5 || langle >157.5){
                                
                                if(i-1>=0 && i+1<=height){
                                        if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                                }

                        }
	
                        if(langle > 22.5 && langle <= 67.5){
                                if((i-1>=0 && j-1>=0) && (i+1 <height && j+1 < width)){
                                        if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[((i+1)*width+j+1)] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                                }
                        }
        
	                if(langle >67.5 && langle <=112.5){
                                if(j-1>=0 && j+1 < width){
                                        if(mag[(i*width+j-1)] >mag[(i*width+j)] || mag[(i*width+j+1)] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                                }
                        }
           
	             if(langle >112.5 && langle <= 157.5){
                                if((i-1>=0 && j+1<width) && (i+1 <height && j-1 >=0)){
                                        if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[((i+1)*width+j-1)] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                                }
                        }
		
	*/	
			
		if(langle <= 22.5 || langle >157.5){
			if ( localidx-1>= 0 && localidx+1<blockDim.x){
				if(A[(localidx-1)*blockDim.y + localidy] > mag[(i*width+j)] || A[(localidx+1)*blockDim.y + localidy] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
			}
			else if ( i-1>=0 && i+1<height){ 
				if(mag[((i-1)*width+j)] >mag[(i*width+j)] || mag[(i+1)*width+j] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
			}
		}
			
		if(langle > 22.5 && langle <= 67.5){
			if ( (localidx-1>=0 && localidy-1>=0) && (localidx+1<blockDim.x && localidy+1<blockDim.y)){	
				if(A[(localidx-1)*blockDim.y + localidy-1] > mag[(i*width+j)] || A[(localidx+1)*blockDim.y + localidy+1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                        }
			else if ((i-1>=0 && j-1>=0) && (i+1 <height && j+1 < width)){ 
				if(mag[((i-1)*width+j-1)] >mag[(i*width+j)] || mag[(i+1)*width+j+1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
			}
		}
		
		if(langle >67.5 && langle <=112.5){
			if (localidy-1>=0 &&  localidy+1<blockDim.y){
                                if(A[(localidx)*blockDim.y + localidy-1] > mag[(i*width+j)] || A[(localidx)*blockDim.y + localidy+1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                        }
			else if ( j-1>=0 && j+1<width){
                                if(mag[(i*width+j-1)] >mag[(i*width+j)] || mag[i*width+j+1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
			}
		}
		
		if(langle >112.5 && langle <= 157.5){
			if ( (localidx-1>=0 && localidy+1<blockDim.y) && (localidx+1<blockDim.x && localidy-1>=0)){
                                if(A[(localidx-1)*blockDim.y + localidy+1] > mag[(i*width+j)] || A[(localidx+1)*blockDim.y + localidy-1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                        }
                        else if ((i-1>=0 && j+1<width) && (i+1 <height && j-1 >=0)){
                                if(mag[((i-1)*width+j+1)] >mag[(i*width+j)] || mag[(i+1)*width+j-1] > mag[(i*width+j)]){
                                                sup[i*width+j] = 0;
                                        }
                        }

		}	
		
	}

}


__global__
void hystersis(float *hys, float *sup, int width, int height, float t_hi, float t_lo){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;
	
	hys[i*width+j] = sup[i*width+j];
	if(i<height && j< width){
		if(sup[i*width+j]>=t_hi){
			hys[i*width+j] = 255;
		}
		else if(sup[i*width+j]<=t_lo){
			hys[i*width+j] = 0;
		}
		else{
			hys[i*width+j] = 125;
		}
	}
}

__global__
void myedges(float *edg, float *hys, int width, int height){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
        int j = threadIdx.y + blockIdx.y*blockDim.y;
	edg[i*width+j] = hys[i*width+j];
	if(i<height && j< width){
		if(hys[i*width+j] == 125){
			edg[i*width+j]=0;
			for(int i_cord=-1; i_cord<2; i_cord++){
                        	for(int j_cord=-1; j_cord<2; j_cord++){
					if(i+i_cord>=0 && i+i_cord<height && j+j_cord>=0 && j+j_cord<width){
                                                        if(hys[(i+i_cord)*width+j+j_cord] == 255){
                                                                edg[i*width+j] = 255;
                                                        }
                                        }
                                 }
                        }
		}
	}
}

int main(int argc, char ** argv){
       	cudaSetDevice(989355990%4); 
	int width, height;
        float *h_image, *h_sup, *h_edg;
        float *d_image, *dG_Kernel, *dGD_Kernel;
        float *dtemp_horizontal, *d_horizontal, *dtemp_vertical, *d_vertical;
        float *d_mag, *d_dir, *d_sup, *d_hys, *d_edg;
        read_image_template<float>(argv[1], &h_image, &width, &height);
        /* start the time counter */
        struct timeval start, stop, convstart, convstop;
        //gettimeofday(&start, NULL);
        /* creat the Gaussian Kernel */
        float *G_Kernel;
        int w;
        Gaussian_Kernel(&G_Kernel,&w,atof(argv[2]));
        /* creat the Gaussian Derivative Kernel */
        float *GD_Kernel;
        Gaussian_Deri_Kernel( &GD_Kernel, &w, atof(argv[2]));
        /* allocates host's and GPU's buffers */
        h_edg = (float*)malloc(sizeof(float)*width*height);
	
	cudaMalloc((void **)&d_image, sizeof(float)*width*height);
        cudaMalloc((void **)&dG_Kernel, sizeof(float)*w);
        cudaMalloc((void **)&dGD_Kernel, sizeof(float)*w);
        cudaMalloc((void **)&dtemp_horizontal, sizeof(float)*width*height);
        cudaMalloc((void **)&d_horizontal, sizeof(float)*width*height);
        cudaMalloc((void **)&dtemp_vertical, sizeof(float)*width*height);
        cudaMalloc((void **)&d_vertical, sizeof(float)*width*height);
        cudaMalloc((void **)&d_mag,sizeof(float)*width*height);
        cudaMalloc((void **)&d_dir,sizeof(float)*width*height);
	cudaMalloc((void **)&d_sup,sizeof(float)*width*height);
	cudaMalloc((void **)&d_hys,sizeof(float)*width*height);
	cudaMalloc((void **)&d_edg,sizeof(float)*width*height);

	/* host transfers data to GPU for processing */
        struct timeval h2dstart,h2dstop;
        gettimeofday(&h2dstart, NULL);
        cudaMemcpy(d_image, h_image, sizeof(float)*width*height, cudaMemcpyHostToDevice);
        cudaMemcpy(dG_Kernel, G_Kernel, sizeof(float)*w, cudaMemcpyHostToDevice);
        cudaMemcpy(dGD_Kernel, GD_Kernel, sizeof(float)*w, cudaMemcpyHostToDevice);
        gettimeofday(&h2dstop,NULL);
        /* call the GPU kernel for processing */
        int blocksize = atoi(argv[3]);
        dim3 dimBlock(blocksize,blocksize);
        dim3 dimGrid(width/blocksize, height/blocksize);
        gettimeofday(&start, NULL);
        gettimeofday(&convstart,NULL);
	/* Horizontal Gradient */
        Convolve<<<dimGrid, dimBlock,sizeof(float)*blocksize*blocksize >>>(d_image, dtemp_horizontal,width,height,w,1,dG_Kernel);
        Convolve<<<dimGrid, dimBlock,sizeof(float)*blocksize*blocksize>>>(dtemp_horizontal,d_horizontal,width,height,1,w,dGD_Kernel);
        cudaDeviceSynchronize();
	/* Vertical Gradient */
        Convolve<<<dimGrid,dimBlock,sizeof(float)*blocksize*blocksize>>>(d_image, dtemp_vertical,width,height,1,w,dG_Kernel);
        Convolve<<<dimGrid,dimBlock,sizeof(float)*blocksize*blocksize>>>(dtemp_vertical, d_vertical,width,height,w,1,dGD_Kernel);
        cudaDeviceSynchronize();
	gettimeofday(&convstop,NULL);

	/* Magnitude */
        struct timeval mdstart,mdstop;
        gettimeofday(&mdstart, NULL);
        magnitude<<<dimGrid,dimBlock>>>(d_mag, d_vertical, d_horizontal,width,height);
        cudaDeviceSynchronize();

	/* Direction */
        direction<<<dimGrid,dimBlock>>>(d_dir, d_vertical, d_horizontal,width, height);
        cudaDeviceSynchronize();
	gettimeofday(&mdstop,NULL);

	/* suppression */
	struct timeval supstart,supstop;
	gettimeofday(&supstart, NULL);
	suppression<<<dimGrid, dimBlock,sizeof(float)*blocksize*blocksize >>>(d_sup, d_mag,d_dir, width,height);
	//cudaMemcpy(h_edg,d_sup,sizeof(float)*width*height,cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();
	gettimeofday(&supstop, NULL);
	

	/* Hysteresis */
	struct timeval Gsstart,Gsstop;
        gettimeofday(&Gsstart, NULL);
	thrust::device_ptr<float> thr_d(d_sup);
	thrust::device_vector<float>d_sup_vec(thr_d,thr_d+(height*width));
	thrust::sort(d_sup_vec.begin(),d_sup_vec.end());
        gettimeofday(&Gsstop, NULL);
	int index = (int)(0.9*height*width);
        float t_hi = d_sup_vec[index];
        float t_lo = t_hi*0.2;

	struct timeval hysstart,hysstop;
        gettimeofday(&hysstart, NULL);
	hystersis<<<dimGrid,dimBlock>>>(d_hys, d_sup, width, height, t_hi, t_lo);
	//cudaMemcpy(h_edg,d_hys,sizeof(float)*width*height,cudaMemcpyDeviceToHost);	
	cudaDeviceSynchronize();
	gettimeofday(&hysstop, NULL);
	/* Edges */
	struct timeval edgstart,edgstop;
        gettimeofday(&edgstart, NULL);
	myedges<<<dimGrid, dimBlock>>>(d_edg, d_hys, width, height);
	gettimeofday(&edgstop, NULL);
	
	struct timeval d2hstart,d2hstop;
	gettimeofday(&d2hstart, NULL);
	cudaMemcpy(h_edg,d_edg,sizeof(float)*width*height,cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();
	gettimeofday(&d2hstop, NULL);
	gettimeofday(&stop,NULL);
	printf("%d, %d, %ld,", width,atoi(argv[3]),(((stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))));
	printf("%ld,", ((convstop.tv_sec * 1000000 + convstop.tv_usec) - (convstart.tv_sec * 1000000 + convstart.tv_usec)));
	printf("%ld,", ((mdstop.tv_sec * 1000000 + mdstop.tv_usec) - (mdstart.tv_sec * 1000000 + mdstart.tv_usec)));
	printf("%ld,", ((supstop.tv_sec * 1000000 + supstop.tv_usec) - (supstart.tv_sec * 1000000 + supstart.tv_usec)));
	printf("%ld,", ((Gsstop.tv_sec * 1000000 + Gsstop.tv_usec) - (Gsstart.tv_sec * 1000000 + Gsstart.tv_usec)));
	printf("%ld,", ((edgstop.tv_sec * 1000000 + edgstop.tv_usec) - (edgstart.tv_sec * 1000000 + edgstart.tv_usec)));
	printf("%ld,", ((hysstop.tv_sec * 1000000 + hysstop.tv_usec) - (hysstart.tv_sec * 1000000 + hysstart.tv_usec)));
	printf("%ld,", ((h2dstop.tv_sec * 1000000 + h2dstop.tv_usec) - (h2dstart.tv_sec * 1000000 + h2dstart.tv_usec)));
	printf("%ld\n", ((d2hstop.tv_sec * 1000000 + d2hstop.tv_usec) - (d2hstart.tv_sec * 1000000 + d2hstart.tv_usec)));
	/* export the results */
	char ename[] ="edges.pgm";
	write_image_template<float>(ename, h_edg, width, height);

	/* free the memory */
        free(h_image);
        free(G_Kernel);
        free(GD_Kernel);

        free(h_edg);
       
        cudaFree(d_image);
        cudaFree(dG_Kernel);
        cudaFree(dGD_Kernel);
        cudaFree(dtemp_horizontal);
        cudaFree(d_horizontal);
        cudaFree(dtemp_vertical);
        cudaFree(d_vertical);
	cudaFree(d_mag);
	cudaFree(d_dir);
	cudaFree(d_sup);
	cudaFree(d_hys);
	cudaFree(d_edg);
	return 0;
}
